$Id 

*******************************************************************************************
Warning
-------

First you need to set up your Payline account, see Payline doc.

Payline doc :

http://www.payline.com/fr/support/documentations.html

*******************************************************************************************

Requirements
------------

This module require at least version 6.x of Drupal and Ubercart 6.x-2.0

Installation
------------
1. Copy the folder named 'uc_paybox' and its contents to the Ubercart contrib modules 
   directory of your Drupal installation
   (for example 'sites/all/modules/ubercart/contrib/').

2. Go to 'admin/build/modules' and enable Payline payment system.

3. Go to 'admin/store/settings/payment/edit/methods' and edit option for the Payline module.

4. Configure your Payline module settings with the same information from Payline administration 
  panel.

5. Test before use on production !.
   Test card VISA number: 4970100000325734
   Validity: > today
   CVV or cryptogramme: 123

6. Test, test , test and report bug !
   you can see a lot of information on log: 'admin/reports/dblog'

*******************************************************************************************

This module has been developed by Mog for arthur-com.net
Sponsored by Emerya

Post a message on the drupal.org site if you have any ideas on 
how we can improve the module.

Mog.
tech@arthura.fr