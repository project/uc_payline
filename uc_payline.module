<?php

/**
 * @file
 * Integrates Payline payment service. http://www.payline.com/
 *
 * Development sponsored by Emerya
 */

/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implementation of hook_menu().
 */
function uc_payline_menu() {

  $items['cart/payline/checkout'] = array(
    'title' => 'Checkout order',
    'page callback' => 'uc_payline_payment',
    'access callback' => 'uc_payline_completion_access',
    'type' => MENU_CALLBACK,
    'file' => 'uc_payline.pages.inc',
  );
  $items['cart/payline/return'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_payline_response',
    'access callback' => 'uc_payline_completion_access',
    'type' => MENU_CALLBACK,
    'file' => 'uc_payline.pages.inc',
  );
  $items['cart/payline/cancel'] = array(
    'title' => 'Order canceled',
    'page callback' => 'uc_payline_response',
    'page arguments' => array(TRUE),
    'access callback' => 'uc_payline_completion_access',
    'type' => MENU_CALLBACK,
    'file' => 'uc_payline.pages.inc',
  );
  $items['cart/payline/notify'] = array(
    'title' => 'Order auto notification',
    'page callback' => 'uc_payline_response',
    'page arguments' => array(TRUE, TRUE),
    'access callback' => 'uc_payline_completion_access',
    'type' => MENU_CALLBACK,
    'file' => 'uc_payline.pages.inc',
  );
  $items['admin/store/orders/payline/%'] = array(
    'title' => 'Get payment detail',
    'page callback' => 'uc_payline_get_details',
    'page arguments' => array(4),
    'access arguments' => array('administer store'),
    'type' => MENU_CALLBACK,
    'file' => 'uc_payline.pages.inc',
  );
  return $items;
}

// Make sure anyone can complete their Payline orders.
function uc_payline_completion_access() {
  return TRUE;
}

/**
 * Implementation of hook_ucga_display().
 */
function uc_payline_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'payline' && arg(2) == 'return') {
    return TRUE;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function uc_payline_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    if ($order->payment_method == 'payline') {
      unset($form['submit']);
      unset($form['back']);
      $form['#action'] = base_path() .'cart/payline/checkout';
      $form['back'] = array(
        '#value' => '<span style="margin-right:200px;">'. l(t('Back'), 'cart/checkout') .'</span>',
      );
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => variable_get('uc_payline_checkout_button', t('Submit Order')),
      );
    }
  }
  elseif ($form_id == 'uc_store_format_settings_form') {
    $form['currency']['uc_currency_code']['#description'] .= ' '. t('Payline only accepts the following currencies: @list', array('@list' => implode(', ', array_keys(_uc_payline_currency(FALSE)))));
  }
}

/*******************************************************************************
 * Hook Functions (Ubercart)
 ******************************************************************************/

/**
 * Implementation of hook_order_actions().
 */
function uc_payline_order_actions($order) {
  $actions = array();
  $module_path = base_path() . drupal_get_path('module', 'uc_payline');
  if (user_access('fulfill orders')) {
    $token = db_result(db_query("SELECT token FROM {uc_payline_token} WHERE order_id = %d", $order->order_id));
    if ($token) {
      $title = t('View payline payment details');
      $actions[] = array(
        'name' => t('Payline payment'),
        'url' => 'admin/store/orders/payline/'. $order->order_id,
        'icon' => '<img src="'. $module_path .'/images/payline.gif" alt="'. $title .'" />',
        'title' => $title,
      );
    }
  }
  return $actions;
}

/**
 * Implementation of hook_payment_method().
 */
function uc_payline_payment_method() {
  $img = drupal_get_path('module', 'uc_payline') .'/images/logo/'. variable_get('uc_payline_method_title_icons', 'payline.jpg');
  $alt = t('Credit card on a secured server by Payline.');
  $title = theme('image', $img, $alt, $alt);
  $methods[] = array(
    'id' => 'payline',
    'name' => t('Credit Card with Payline'),
    'title' => $title . variable_get('uc_payline_method_title', t('Credit card on a secured server by Payline.')),
    'review' => t('Credit Card with Payline'),
    'desc' => variable_get('uc_payline_method_descr', t('Redirect to Payline to pay by credit card.')),
    'callback' => 'uc_payment_method_payline',
    'weight' => 0,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );
  return $methods;
}

/**
 * Implementation of hook_store_status().
 *
 * Currently gives some infos/warning about uc_payline configuration
 */
function uc_payline_store_status() {
  $statuses = array();
  // Warning about "no ID"
  if (variable_get('uc_payline_id', '') == '') {
    $statuses[] = array(
      'status' => 'warning',
      'title' => t('Payline configuration'),
      'desc' => t('No merchant ID defined.'),
    );
  }
  // Error about the key
  if (variable_get('uc_payline_key', '') == '') {
    $statuses[] = array(
      'status' => 'error',
      'title' => t('Payline configuration'),
      'desc' => t('No merchant KEY defined.'),
    );
  }
  return $statuses;
}

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

/**
 * Callback for payline payment method settings.
 */
function uc_payment_method_payline($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
      $details = variable_get('uc_payline_method_descr', t('Redirect to Payline to pay by credit card.'));
      return check_markup($details, FILTER_FORMAT_DEFAULT, FALSE);

    case 'settings':
      $url = _uc_payline_url();
      $urlauto = $url .'cart/payline/auto';
      $form['uc_payline_text'] = array(
        '#type' => 'item',
        '#prefix' => '<div class="help">',
        '#value' => t('First you need to configure your Payline account from <a href="!url">!url</a>.',
          array('!url' => 'https://homologation-admin.payline.com')),
        '#suffix' => '</div>',
      );
      $form['id'] = array(
        '#type' => 'fieldset',
        '#title' => t('Payline identification'),
        '#description' => t('This information <b>must be the same</b> as those of your Payline administration interface.'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['id']['uc_payline_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Payline Merchant ID'),
        '#description' => t('You must enter the merchant ID you received when you registered.'),
        '#default_value' => variable_get('uc_payline_id', ''),
        '#required' => TRUE,
        '#size' => 16,
      );
      $form['id']['uc_payline_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Payline Certificate key'),
        '#description' => t('You must enter the key dealer who has been notified during registration. If you have lost your access key, you can regenerate a new one on the Payline administration'),
        '#default_value' => variable_get('uc_payline_key', ''),
        '#required' => TRUE,
        '#size' => 22,
      );
      $form['id']['uc_payline_mode'] = array(
        '#type' => 'checkbox',
        '#title' => t('Production mode'),
        '#description' => t('If checked, Payline will be set in production mode. You need to test this payment before you can activate Production mode.'),
        '#default_value' => variable_get('uc_payline_mode', FALSE),
      );
      $form['id']['uc_payline_contrat'] = array(
        '#type' => 'textfield',
        '#title' => t('Payline contract number'),
        '#description' => t('You must enter your VAD contract number that you have filled in the activation of a payment to your merchant account'),
        '#default_value' => variable_get('uc_payline_contrat', ''),
        '#required' => TRUE,
        '#size' => 10,
      );
      $form['id']['uc_payline_contrat_liste'] = array(
        '#type' => 'textfield',
        '#title' => t('Payline contract number list'),
        '#description' => t("You must enter all your VAD contract number you wish to activate, seprarate each with ';'. Leave empty if you get one contrat already set."),
        '#default_value' => variable_get('uc_payline_contrat_liste', ''),
        '#size' => 60,
      );
      $form['uc_payline_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#description' => t('Payment title used on checkout page'),
        '#default_value' => variable_get('uc_payline_method_title', t('Credit card on a secured server by Payline.')),
      );
      $form['uc_payline_method_descr'] = array(
        '#type' => 'textarea',
        '#title' => t('Payment method description'),
        '#default_value' => variable_get('uc_payline_method_descr', t('Redirect to Payline to pay by credit card.')),
      );
      $form['uc_payline_method_title_icons'] = array(
        '#type' => 'select',
        '#title' => t('Icon on payment title'),
        '#description' => t('Images are taken from <em>uc_payline/images/logo</em>. Will be on the left of the title.'),
        '#options' => _uc_payline_list_img(),
        '#default_value' => variable_get('uc_payline_method_title_icons', 'payline.jpg'),
      );
      $form['uc_payline_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Ubercart order review submit button text'),
        '#description' => t('Provide Payline specific text for the submit button on the order review page.'),
        '#default_value' => variable_get('uc_payline_checkout_button', t('Submit Order')),
        '#required' => TRUE,
        '#size' => 40,
      );
      $form['payline'] = array(
        '#type' => 'fieldset',
        '#title' => t('Payline administration setup'),
        '#description' => t('This information <b>must be the same</b> as those of your Payline administration interface.'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['payline']['uc_payline_language'] = array(
        '#type' => 'select',
        '#title' => t('Language preference'),
        '#description' => t('Adjust language on Payline pages.'),
        '#options' => _uc_payline_language(),
        '#default_value' => variable_get('uc_payline_language', 'fre/fra'),
      );
      $form['payline']['uc_payline_devise'] = array(
        '#type' => 'select',
        '#title' => t('Currency'),
        '#description' => t('Select currency. Ubercart does not use multiple currency, you need to set up this options according to <a href="@url">ubercart shop configuration</a>.', array('@url' => base_path() .'admin/store/settings/store/edit/format')),
        '#options' => _uc_payline_currency(),
        '#default_value' => variable_get('uc_payline_devise', '978'),
      );
      $form['payline']['uc_payline_page_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment custom page code'),
        '#description' => t('If you create a custom payment page on Payline website, you can fill in the id of this page.'),
        '#default_value' => variable_get('uc_payline_page_code', ''),
        '#size' => 50,
      );
      $form['payline_advanced'] = array(
        '#type' => 'fieldset',
        '#title' => t('Payline advanced configuration'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['payline_advanced']['uc_payline_action'] = array(
        '#type' => 'select',
        '#title' => t('Payment action code'),
        '#options' => array(100 => 'Authorization', 101 => 'Authorization + validation'),
        '#default_value' => variable_get('uc_payline_action', 101),
      );
      $form['payline_advanced']['uc_payline_origin'] = array(
        '#type' => 'textfield',
        '#title' => t('Origin of Order'),
        '#default_value' => variable_get('uc_payline_origin', ''),
        '#size' => 50,
      );
      $form['payline_advanced']['uc_payline_proxy_host'] = array(
        '#type' => 'textfield',
        '#title' => t('Proxy url'),
        '#default_value' => variable_get('uc_payline_proxy_host', ''),
        '#size' => 40,
      );
      $form['payline_advanced']['uc_payline_proxy_port'] = array(
        '#type' => 'textfield',
        '#title' => t('Proxy port'),
        '#default_value' => variable_get('uc_payline_proxy_port', ''),
        '#size' => 10,
      );
      $form['payline_advanced']['uc_payline_proxy_login'] = array(
        '#type' => 'textfield',
        '#title' => t('Proxy login'),
        '#default_value' => variable_get('uc_payline_proxy_login', ''),
        '#size' => 40,
      );
      $form['payline_advanced']['uc_payline_proxy_password'] = array(
        '#type' => 'textfield',
        '#title' => t('Proxy password'),
        '#default_value' => variable_get('uc_payline_proxy_password', ''),
        '#size' => 40,
      );
      $form['payline_advanced']['uc_payline_debug'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable debug mode'),
        '#default_value' => variable_get('uc_payline_debug', FALSE),
      );
      return $form;
  }
}

/**
 * helper to create proper url
 */
function _uc_payline_url() {
  $proto = $_SERVER['HTTPS'] ? 'https://' : 'http://';
  $host = $_SERVER['SERVER_NAME'];
  $port = ($_SERVER['SERVER_PORT'] == 80 ? '' : ':'. $_SERVER['SERVER_PORT']);
  return $proto . $host . $port . base_path();
}

/**
 * helper to list image from module directory
 */
function _uc_payline_list_img($rep = 'logo')  {
  $dir = drupal_get_path('module', 'uc_payline') .'/images/'. $rep;
  $rep == 'logo' ? $liste = array(0 => t('no logo')) : '' ;
  if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
      while (($file = readdir($dh)) !== FALSE) {
        if ( "." == $file || ".." == $file ) {
          continue;
        }
        else {
          // if get folder from windows system
          $file != 'Thumbs.db' ? $liste[$file] = $file : '';
        }
      }
      closedir($dh);
    }
  }
  return $liste;
}

/**
 * Define language available and check, see payline doc
 */
function _uc_payline_language($lang = FALSE) {
  $output = array(
    'fre/fra' => t('French'),
    'ger' => t('Dutch'),
    'eng' => t('English'),
    'spa' => t('Spanish'),
    'ita' => t('Italian'),
    'por' => t('Portuguese'),
  );
  if ($lang) {
    in_array($lang, $output) ? $output = $lang : $output = $output;
  }
  return $output;
}

/**
 * Define currency available and check, see payline doc
 */
function _uc_payline_currency() {
  $payline_currency = array(
    '978' => t('Euro (EUR)'),
    '840' => t('American Dollar (USD)'),
    '756' => t('Swiss Franc'),
    '826' => t('Pound Sterling'),
    '124' => t('Canadian Dollar'),
  );
  return $payline_currency;
}
