<?php

/**
 * @file
 * payline menu items.
 *
 * Development sponsored by Emerya
 */

require_once(drupal_get_path('module', 'uc_payline') .'/lib/payline.php');

DEFINE( 'MERCHANT_ID', variable_get('uc_payline_id', '') ); // Merchant ID
DEFINE( 'ACCESS_KEY', variable_get('uc_payline_key', '') ); // Certificate key
DEFINE( 'PROXY_HOST', variable_get('uc_payline_proxy_host', NULL) ); // Proxy URL (optional)
DEFINE( 'PROXY_PORT', variable_get('uc_payline_proxy_port', NULL) ); // Proxy port number without 'quotes' (optional)
DEFINE( 'PROXY_LOGIN', variable_get('uc_payline_proxy_login', '') ); // Proxy login (optional)
DEFINE( 'PROXY_PASSWORD', variable_get('uc_payline_proxy_password', '') ); // Proxy password (optional)
DEFINE( 'PRODUCTION', variable_get('uc_payline_mode', FALSE) ); // Demonstration (FALSE) or production (TRUE) mode
DEFINE( 'CUSTOM_PAYMENT_TEMPLATE_URL', '');
DEFINE( 'CUSTOM_PAYMENT_PAGE_CODE', variable_get('uc_payline_page_code', '') );
DEFINE( 'CONTRACT_NUMBER_LIST', variable_get('uc_payline_contrat_liste', '') );

 /**
 * Return payline form to redirect payline payment.
 */
function uc_payline_payment() {

  if (!isset($_SESSION['cart_order'])) {
    drupal_set_message(t('You are not allowed to see this page, please, return to homepage.'));
    drupal_goto('cart');
  }

  global $user;
  $array = array();
  $payline = new paylineSDK();
  $order = uc_order_load($_SESSION['cart_order']);

  // Paiement
  $array['payment']['amount'] = round($order->order_total, 2) * 100;
  $array['payment']['currency'] = variable_get('uc_payline_devise', '978');
  // guide integration p131 chap 9.9
  $array['payment']['action'] = variable_get('uc_payline_action', 101);
  // CPT = coptant, DIF = differe, NX = n fois, REC = recurent (guide p132 9.10)
  $array['payment']['mode'] =  'CPT';
  // si mode DIF
  $array['payment']['differedActionDate'] = NULL ;
  // numero de contrat
  $array['payment']['contractNumber'] = variable_get('uc_payline_contrat', '0001');

  // Commande
  $array['order']['ref'] = $order->order_id;
  $array['order']['country'] = 'FR';
  $array['order']['amount'] = round($order->order_total, 2) * 100;
  $array['order']['date'] = date("d/m/Y H:i");
  $array['order']['currency'] = variable_get('uc_payline_devise', '978');
  $taxes = 0;
  foreach($order->line_items AS $item) {
    if ($item['type'] == 'tax') {
      $taxes += $item['amount'];
    }
  }
  $array['order']['taxes'] = round($taxes,2) * 100;
  $array['order']['origin'] = variable_get('uc_payline_origin', '');

  // Client
  $array['buyer']['lastName'] = $order->billing_first_name;
  $array['buyer']['firstName'] = $order->billing_last_name;
  $array['buyer']['email'] = $order->primary_email;

  // Adresse client
  //$array['address']['name'] = $_POST['addressName'];
  $array['address']['street1'] = $order->delivery_street1;
  $array['address']['street2'] = $order->delivery_street2;
  $array['address']['cityName'] = $order->delivery_city;
  $array['address']['zipCode'] = $order->delivery_postal_code;
  $array['address']['country'] = $order->delivery_country;
  $array['address']['phone'] =  $order->delivery_phone;

  // Detail des produits
  foreach ($order->products as $product) {
    $item = array();
    $item['ref'] = $product->nid;
    $item['price'] = round($product->price, 2) * 100;
    $item['quantity'] = $product->qty;
    if (is_array($product->data['attributes']) && count($product->data['attributes']) > 0) {
      foreach ($product->data['attributes'] as $attribute => $option) {
        $product->comment = t(' @attribute: @options', array('@attribute' => $attribute, '@options' => implode(', ', (array)$option)));
      }
    }
    $item['comment'] = $product->title . $product->comment;
    // ajout du produit
    $payline->setItem($item);
  }

  // Passage info id commande pour gestion retour
  $$privatedata1 = array();
  $privatedata1['key'] = 'order_id';
  $privatedata1['value'] = $order->order_id;
  $payline->setPrivate($privatedata1);

  // Options de transaction
  $array['notificationURL'] = _uc_payline_url() . 'cart/payline/notify';
  $array['returnURL'] =  _uc_payline_url() . 'cart/payline/return';
  $array['cancelURL'] =  _uc_payline_url() . 'cart/payline/cancel';
  $array['languageCode'] = variable_get('uc_payline_language', 'fra');
  $array['securityMode'] = 'SSL';

  // gestion des templates
  $array['customPaymentTemplateURL'] = CUSTOM_PAYMENT_TEMPLATE_URL;
  $array['customPaymentPageCode'] = CUSTOM_PAYMENT_PAGE_CODE;

  // Liste des contrats disponibles
  $contracts = split(";", variable_get('uc_payline_contrat_liste', ''));
  $array['contracts'] = $contracts;

  // Creation du paiement
  $result = $payline->do_webpayment($array);

  // debug
  if (variable_get('uc_payline_debug', FALSE) == TRUE) {
    drupal_set_message('<pre>Order: '. print_r($order, TRUE) .'</pre>');
    drupal_set_message('<pre>Request: '. print_r($payline->do_webpayment($array, 1), TRUE) .'</pre>');
    drupal_set_message('<pre>Response: '. print_r($result, TRUE) .'</pre>');
    return '';
  }
  else{
    if (isset($result) && $result['result']['code'] == '00000') {
      // tout est ok, ajout token
      db_query("INSERT INTO {uc_payline_token} (order_id, token) VALUES (%d, '%s')", $order->order_id, $result['token']);
      // redirection vers payline
      header("location:". $result['redirectURL']);
      // pas de theme !
      exit();
    }
    elseif (isset($result)) {
      drupal_set_message(t('An error occured during the payment process, please contact us.'));
      drupal_set_message(t('ERROR') .' : '. $result['result']['code'] . ' '. $result['result']['longMessage']);
      watchdog('uc_payline', 'Payment error order !order_id: @error - @error_long', array('@error_long' => $result['result']['longMessage'], '@error' => $result['result']['code'], '!order_id' => $order_id), WATCHDOG_WARNING);
      drupal_goto('cart');
    }
  }
}

 /**
 * Customer return after payline validation
 */
function uc_payline_response($cancel = FALSE, $auto = FALSE) {

  global $user;
  $payline = new paylineSDK();

  // recuperation du token de la commande
  if (isset($_POST['token'])) {
      $token = $_POST['token'];
  }
  elseif (isset($_GET['token'])) {
      $token = $_GET['token'];
  }
  else {
    drupal_set_message(t('You are not allowed to see this page, please, return to homepage.'));
    watchdog('uc_payline', 'Error: access response page with no token, ip: @ip', array('@ip' => ip_address()), WATCHDOG_WARNING);
    drupal_goto('cart');
  }

  // response depuis payline
  $response = $payline->get_webPaymentDetails($token);

  // debug
  if (variable_get('uc_payline_debug', FALSE) == TRUE) {
    drupal_set_message('<pre>Response: '. print_r($response, TRUE) .'</pre>');
  }

  // traitement de la reponse
  $code = $response['result']['code'];
  $order_id = $response['privateDataList']['privateData']['value'];
  $order = uc_order_load($order_id);

  if ($order) {
    $check_token = db_result(db_query("SELECT token FROM {uc_payline_token} WHERE order_id = %d", $order_id));

    if (strcmp($check_token, $token) == 0) {
      if ($auto) {
        return uc_payline_auto($order, $response);
      }
      elseif ($cancel) {
        uc_payline_cancel($order);
      }
      elseif ($code == '00000') {
        return uc_payline_complete($order, $response);
      }
      else {
        uc_payline_error($order, $response);
      }
    }
    else {
      drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
      watchdog('uc_payline', "Token error on order !id: !token1 / !token2.", array('!id' => $order_id, '!token1' => $token, '!token2' => $check_token), WATCHDOG_WARNING, NULL);
      drupal_goto('cart');
    }
  }
  else {
    drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
    watchdog('uc_payline', "No order_id found. token: !token", array('!token' => $token), WATCHDOG_WARNING, NULL);
    drupal_goto('cart');
  }
}

 /**
 * Handle payline payment validation
 */
function uc_payline_complete($order, $response) {

  // Check status
  if ($order->order_status != 'in_checkout' && $order->order_status != 'pending') {
    $errors[] = t('Order not in checkout or pending: ') . $order->order_status;
  }
  if ($order->order_status == 'payment_received') {
    $errors[] = t('Try to revalide a payment.');
  }
  if (!empty($errors)) {
    // Log errors to order and watchdog.
    $error_data = array('!id' => $order->order_id, '@error' => implode(" - ", $errors));
    uc_order_comment_save($order->order_id, 0, t('Order !id caused error: @error', $error_data), 'admin');
    watchdog('uc_payline', 'Order !id caused error: @error', $error_data, WATCHDOG_ERROR);
    // debug
    if (variable_get('uc_payline_debug', FALSE) == TRUE) {
      $output = t('Order !id caused error: @error', $error_data);
      print $output;
    }
    drupal_goto('cart');
  }

  // tout est ok, validation de la commande
  $variables = array(
                '!tid' => $response['transaction']['id'],
                '!tdate' => $response['transaction']['date'],
                '!auth_id' => $response['authorization']['number'],
              );  
  $comment = t('Paid by payline transaction: !tid - !tdate authorization: !auth_id.', $variables);
  // validation du paiement
  uc_payment_enter($order->order_id, 'uc_payline', $response['payment']['amount'] / 100, 0, NULL, $comment);
  // commentaires
  uc_order_comment_save($order->order_id, 0, t('Order created through website. Payline transaction id: !tid', $variables));
  uc_order_comment_save($order->order_id, 0, t('Payment accepted. Payline transaction id: !tid', $variables), 'order');
  // validation de la commande et redirection
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  // check if alternate checkout completion page is set
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }
  return $output;
}


 /**
 * Handle payline cancel payment
 */
function uc_payline_cancel($order) {

  global $user;
  unset($_SESSION['cart_order']);
  uc_order_comment_save($order->order_id, $user->uid, t('This order has been explicitly canceled by the user.'), 'order');

  if (uc_order_update_status($order->order_id, uc_order_state_default('canceled'))) {
      drupal_set_message(t('Your order has been canceled.'), 'status');
  }
  else {
    drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
    watchdog('uc_payline', "The order @order couldn't be marked as 'Canceled'.", array('@order' => $order->order_id), WATCHDOG_WARNING, NULL);
  }
  drupal_goto('cart');
}

 /**
 * Handle payline error
 */
function uc_payline_error($order, $response) {
  $errors = array('!id' => $order->order_id, '!code' => $response['result']['code'], '@msg' => $response['result']['longMessage']);
  // admin comment
  uc_order_comment_save($order->order_id, 0, t('Error [!code] order !id: @msg.', $errors));
  watchdog('uc_payline', "Error [!code] order !id: @msg.", $errors, WATCHDOG_WARNING, NULL);
  drupal_set_message(t('An error has occured during payment, please try again.'), 'error');
  drupal_goto('cart');
}

 /**
 * auto notification from payline
 */
function uc_payline_auto($order, $response) {
  $montant = $response['payment']['amount'];

  // check order
  if (!$order) {
    $errors[] = t('Invalid order !id.', array('!id' => $order->order_id));
  }
  else {
    // Check status
    if ($order->order_status != 'in_checkout' && $order->order_status != 'pending') {
      $errors[] = t('Order not in checkout or pending: ') . $order->order_status;
    }
    if ($order->order_status == 'payment_received') {
      $errors[] = t('Try to revalide a payment.');
    }
    // Check price
    if ($order->order_total*100 != $montant) {
      $errors[] = t('Incorrect price: @price1 / @price2.', array('@price1' => $order->order_total*100, '@price2' => $montant));
    }
    // Check error code
    if ($erreur != "00000") {
      $errors[] = t('Error [!code] order !id: @msg.', array('!id' => $order->order_id, '!code' => $response['result']['code'], '@msg' => $response['result']['longMessage']));
    }
  }
  if (!empty($errors)) {
    // Log errors to order and watchdog.
    $error_data = array('!id' => $order->order_id, '@error' => implode(" - ", $errors));
    // admin comment
    uc_order_comment_save($order->order_id, 0, t('Autoresponse error on order !id: @error', $error_data));
    watchdog('uc_payline', 'Order !id autoresponse caused error: @error', $error_data, WATCHDOG_ERROR);

    // debug
    if (variable_get('uc_payline_debug', FALSE) == TRUE) {
      $output = t('Order !id caused error: @error', $error_data);
      print $output;
    }
    exit();
  }

  // if everything ok, add comment (later: use auto to handle recurring payment)
  $comment = t('Payline notify transaction: @tid - @tdate authorization: @auth_id.',
              array(
                '!tid' => $response['transaction']['id'],
                '!tdate' => $response['transaction']['date'],
                '!auth_id' => $response['authorization']['number'],
              ));
  // admin comment
  uc_order_comment_save($order->order_id, 0, $comment);
  // debug
  if (variable_get('uc_payline_debug', FALSE) == TRUE) {
    print $comment;
  }
  exit();
}

/**
 * Get payment detail form
 */
function uc_payline_get_details($order_id) {
  if ($order_id) {
    drupal_set_title(t('Get payment detail on order !oid', array('!oid' => $order_id)));
    $token = db_result(db_query("SELECT token FROM {uc_payline_token} WHERE order_id = %d", $order_id));
    $output = t('Token:  !token', array('!token' => $token));
    $payline = new paylineSDK();
    $response = $payline->get_webPaymentDetails($token);
    if (isset($response)) {
      foreach ($response as $data => $resp) {
        $output .= '<h2>'. $data .'</h2>';
        foreach ($resp as $k => $v) {
          is_array($v) ? $output .= '<h3>'. $k .'</h3>'. implode(' :', $v) .'<br/>' : $output .= $k .' : '. $v .'<br/>';
        }
      }
    }
    else {
      drupal_set_message(t('Sorry, no answer found with this token: !token', array('!token' => $token)), 'error');
    }
    $output .= l('retour', 'admin/store/orders/view');
    return $output;
  }
  else {
    drupal_goto('admin/store/orders/view');
  }
}
